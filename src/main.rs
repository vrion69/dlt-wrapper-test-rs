
use dlt_wrapper_rs::{DltLogger, DltLogLevelType, dlt_println, dlt_fatal, dlt_err, dlt_warn, dlt_info, dlt_dbg, dlt_verbose};

#[derive(Debug, Default, Copy, Clone)]
struct Point {
    x: i32,
    y: i32,
}

fn main() {
    let mut logger = DltLogger::new("APP1", "CONTEXT1");

    dlt_println!(logger, DltLogLevelType::DLT_LOG_INFO, "This is a static message\n");
    dlt_println!(logger, DltLogLevelType::DLT_LOG_INFO, "This is a formatted message with value: {}\n", 32);
    let point = Point { x: 1, y: 2 };
    dlt_println!(logger, DltLogLevelType::DLT_LOG_ERROR, "Test point {:?}\n", point);
    dlt_println!(logger, DltLogLevelType::DLT_LOG_WARN, "Hello from rust {:#?}\n", point);

    dlt_fatal!(logger, "This is a fatal message {:?}\n", point);
    dlt_err!(logger, "This is an error message {:?}\n", point);
    dlt_warn!(logger, "This is a warn message {:?}\n", point);
    dlt_info!(logger, "This is an info message {:?}\n", point);
    dlt_dbg!(logger, "This is a debug message {:?}\n", point);
    dlt_verbose!(logger, "This is a verbose message {:?}\n", point);
}
